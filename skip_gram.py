# Importing the libraries
import numpy as np 
import tensorflow as tf 
import pandas as pd
from functools import reduce
import operator
import re 

# importing the dataset
dataset = pd.read_csv('md.tsv', delimiter = '\t', quoting = 3)

# Cleaning the texts
def gen_corpus():
    corpus = []
    for i in range(0, 30314):
        question = re.sub('[^a-zA-Z]', ' ', str(dataset['Title'][i]))
        question = question.lower()
        question = question.split()
        corpus.append(question)

    corpus = reduce(operator.concat, corpus)
    return corpus

        

