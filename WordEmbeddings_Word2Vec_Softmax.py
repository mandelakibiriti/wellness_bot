
# coding: utf-8

# In[14]:


# Importing the libraries
import numpy as np 
import tensorflow as tf 
import pandas as pd
from functools import reduce
import operator
import re 
import collections
import random


# In[2]:


# importing the dataset
dataset = pd.read_csv('md.tsv', delimiter = '\t', quoting = 3)


# In[3]:


dataset


# In[4]:


# Cleaning the texts and creating a vocabulary list
def gen_corpus():
    corpus = []
    for i in range(0, 30314):
        question = re.sub('[^a-zA-Z]', ' ', str(dataset['Title'][i]))
        question = question.lower()
        question = question.split()
        corpus.append(question)

    corpus = reduce(operator.concat, corpus)
    return corpus


# In[5]:


vocabulary = gen_corpus()


# In[6]:


vocabulary


# In[7]:


vocabulary[:25]


# In[8]:


len(vocabulary)


# In[10]:


# Bulding the dataset in a format that will be usefull when generating the word to vec embeddings
# Embeddings will be generated only for the top(n) most frequently used words

def build_dataset(words, n_words):
    # Lists of lists holding words and the count of how many times it appears in the dataset
    # Words not in the top (n) will be added to the unknown count
    word_counts = [['UNKNOWN', -1]] 
    
    # Access the most frequently used words
    counter = collections.Counter(words)
    
    # Mostly frequently used words
    word_counts.extend(counter.most_common(n_words - 1))
    
    # The words are to be fed into a neural network inorder to generate word embeddings which
    # Which can only accept numeric data
    # Higher frequency words will have lower indexes
    dictionary = dict()
    for word, _ in word_counts:
        dictionary[word] = len(dictionary)
    
    word_indexes = list()
    
    unknown_count = 0
    for word in words:
        if word in dictionary:
            index = dictionary[word]
        else:
            index = 0 # dictionary['UNKNOWN]
            unknown_count += 1
        word_indexes.append(index)
    word_counts[0][1] = unknown_count
    reversed_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return word_counts, word_indexes, dictionary, reversed_dictionary


# In[11]:


VOCABULARY_SIZE = 5000
word_counts, word_indexes, dictionary, reversed_dictionary = build_dataset(vocabulary, VOCABULARY_SIZE)


# In[12]:


word_counts[:10]


# In[13]:


word_indexes[:10]


# In[16]:


for key in random.sample(list(dictionary), 10):
    print(key, ":", dictionary[key])


# In[18]:


for key in random.sample(list(reversed_dictionary), 10):
    print(key, ":", reversed_dictionary[key])

